import { Injectable } from '@angular/core';
import {
  HandleError,
  HttpErrorHandler,
} from '../../common/services/http-error-handler/http-error-handler.service';
import { of } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ListingService {
  handleError: HandleError;
  projectList: { docs: any[]; length: number; offset: number } = {
    docs: [
      {
        name: 'Main Milestone',
        uuid: '1',
        route: 'main_milestones',
        rt: '',
      },
      {
        name: 'Project MileStone 1',
        uuid: 'type 1',
        route: 'milestones',
        rt: '0',
      },
      {
        name: 'Project MileStone 2',
        uuid: 'type 2',
        route: 'milestones',
        rt: '1',
      },
    ],
    length: 5,
    offset: 0,
  };
  constructor(httpErrorHandler: HttpErrorHandler) {
    this.handleError = httpErrorHandler.createHandleError('ListingService');
  }

  findModels(
    model: string,
    filter = '',
    sortOrder = 'asc',
    pageNumber = 0,
    pageSize = 10,
  ) {
    // let baseUrl = this.storageService.getInfo(FRAPPE_INSTANCE_URL) || "https://default.mntechnique.com";

    // const url = `${baseUrl}/api/resource/${model}`;
    // const headers: any = {};
    // headers[AUTHORIZATION] = BEARER + this.storageService.getInfo(ACCESS_TOKEN);
    // headers[CONTENT_TYPE] = APPLICATION_JSON_HEADER;
    // headers[ACCEPT] = APPLICATION_JSON_HEADER;
    // headers["Access-Control-Allow-Origin"] = "*"

    // return this.http.get(url, { headers });
    return of(this.projectList);
  }
}
