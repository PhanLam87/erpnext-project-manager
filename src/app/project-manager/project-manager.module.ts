import { NgModule } from '@angular/core';
import { SettingsComponent } from './settings/settings.component';
import { SharedImportsModule } from '../shared-imports/shared-imports.module';
import { CommonModule } from '@angular/common';
import { MainMilestonesComponent } from './main-milestones/main-milestones.component';
import { MilestonesComponent } from './milestones/milestones.component';
import { InlineEditComponent } from './inline-edit/inline-edit.component';

@NgModule({
  declarations: [
    SettingsComponent,
    MainMilestonesComponent,
    MilestonesComponent,
    InlineEditComponent,
  ],
  imports: [CommonModule, SharedImportsModule],
  providers: [],
  exports: [
    SettingsComponent,
    MainMilestonesComponent,
    MilestonesComponent,
    InlineEditComponent,
  ],
})
export class ProjectManagerModule {}
