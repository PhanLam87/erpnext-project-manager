import { Component, OnInit } from '@angular/core';
import { DataSource } from '@angular/cdk/table';
import { BehaviorSubject, Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-milestones',
  templateUrl: './milestones.component.html',
  styleUrls: ['./milestones.component.css'],
})
export class MilestonesComponent implements OnInit {
  milestoneFields = [1, 2, 3, 4, 5];
  milestoneSteps = [];
  i = 0;
  displayedColumns = [
    'index',
    'date',
    'submitAttachment',
    'receiveAttachment',
    'status',
    'remark',
  ];
  dataSource = new ExampleDataSource(initialData);

  constructor(private readonly activatedRoute: ActivatedRoute) {
    if (this.activatedRoute.snapshot.params.id === '0') {
      this.milestoneSteps = [1, 2, 3, 4, 5];
    } else {
      this.milestoneSteps = [1, 2, 3];
    }
  }

  ngOnInit() {}

  addRow() {
    const data = this.dataSource.data();
    data.push({
      status: 'test_status',
      remark: 'test_remark',
      date: 'test_data',
    });
    this.dataSource.update(data);
  }

  update(el: Element, submitAttachment: string) {
    if (submitAttachment == null) {
      return;
    }
    // copy and mutate
    const copy = this.dataSource.data().slice();
    el.submitAttachment = submitAttachment;
    this.dataSource.update(copy);
  }
}

export interface Element {
  submitAttachment?: string;
  receiveAttachment?: string;
  status?: string;
  remark?: string;
  date?: string;
}

const initialData: Element[] = [
  {
    status: 'Needs Approval',
    remark: 'Fair',
    date: 'Urgent product',
  },
  {
    status: 'Incomplete',
    remark: 'needs progress',
    date: 'Customizations',
  },
  {
    status: 'In Progress',
    remark: 'needs specs',
    date: 'CQRS architecture',
  },
];

export class ExampleDataSource extends DataSource<any> {
  private dataSubject = new BehaviorSubject<Element[]>([]);

  data() {
    return this.dataSubject.value;
  }

  update(data) {
    this.dataSubject.next(data);
  }

  constructor(data: any[]) {
    super();
    this.dataSubject.next(data);
  }

  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<Element[]> {
    return this.dataSubject;
  }

  disconnect() {}
}
