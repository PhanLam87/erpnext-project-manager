import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { ACCESS_TOKEN, FRAPPE_INSTANCE_URL } from '../../constants/storage';
import { CLOSE } from '../../constants/messages';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css'],
})
export class SettingsComponent implements OnInit {
  accessToken: string;
  frappeInstanceUrl: string;
  hideClientSecret = true;

  settingsForm = new FormGroup({
    accessToken: new FormControl(this.accessToken),
    frappeInstanceUrl: new FormControl(this.frappeInstanceUrl),
  });

  constructor(private snackBar: MatSnackBar) {}

  ngOnInit() {
    this.settingsForm.controls.accessToken.setValue(
      localStorage.getItem(ACCESS_TOKEN),
    );
    this.settingsForm.controls.frappeInstanceUrl.setValue(
      localStorage.getItem(FRAPPE_INSTANCE_URL),
    );
  }

  updateSettings() {
    localStorage.setItem(
      FRAPPE_INSTANCE_URL,
      this.settingsForm.controls.frappeInstanceUrl.value ||
        'https://default.mntechnique.com',
    );
    localStorage.setItem(
      ACCESS_TOKEN,
      this.settingsForm.controls.accessToken.value,
    );
    this.snackBar.open('Settings updated successfully', CLOSE, {
      duration: 3000,
    });
  }
}
